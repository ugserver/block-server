#/bin/bash
bin=$(cd `dirname $0`;pwd)
echo $bin
cd $bin
echo "------------------"$bin
mvn clean package -Dmaven.test.skip=true

scp target/wallet-server-exec.jar touclick@192.168.5.5:/home/touclick/ugchain/wallet-server
ssh root@192.168.5.5 "/home/touclick/ugchain/wallet-server/restart.sh"

