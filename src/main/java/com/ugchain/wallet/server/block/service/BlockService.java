package com.ugchain.wallet.server.block.service;

import java.io.IOException;

  
/**  
* @ClassName: BlockService  
* @Description: TODO(区块同步数据库服务层接口)  
* @author OldDriver  
* @date 2017年6月5日  
*    
*/  
    
public interface BlockService {

	public void queryBlock() throws IOException, InterruptedException;
	
	void init();
}
