package com.ugchain.wallet.server.block.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


  
/**  
* @ClassName: BlockConfig  
* @Description: TODO(区块同步相关配置)  
* @author OldDriver  
* @date 2017年7月3日  
*    
*/  
    
@Configuration
@ConfigurationProperties(prefix = "block")
public class BlockConfig {

    private String ugTokenAddress;

    private String gethHttpURL;

    private String input;

    private String difference;
    
    
    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    private String redisPort;

    public String getGethHttpURL() {
	return this.gethHttpURL;
    }

    public void setGethHttpURL(String gethHttpURL) {
	this.gethHttpURL = gethHttpURL;
    }

    public String getUgTokenAddress() {
	return ugTokenAddress;
    }

    public void setUgTokenAddress(String ugTokenAddress) {
	this.ugTokenAddress = ugTokenAddress;
    }

    public String getRedisPort() {
	return redisPort;
    }

    public void setRedisPort(String redisPort) {
	this.redisPort = redisPort;
    }
}
