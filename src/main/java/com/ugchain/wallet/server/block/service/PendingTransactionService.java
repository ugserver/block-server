package com.ugchain.wallet.server.block.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.web3j.protocol.core.methods.response.Transaction;

import com.ugchain.wallet.server.block.vo.TransactionRepo;

  
/**  
* @ClassName: PendingTransactionService  
* @Description: TODO(同步pending状态交易服务层)  
* @author OldDriver  
* @date 2017年6月12日  
*    
*/  
    
public interface PendingTransactionService {

    void saveTransaction(Transaction result);
    
}
