package com.ugchain.wallet.server.block.service;

import static org.web3j.tx.ManagedTransaction.GAS_PRICE;

import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlock.Block;
import org.web3j.protocol.core.methods.response.EthBlock.TransactionResult;
import org.web3j.protocol.core.methods.response.EthBlockNumber;
import org.web3j.protocol.core.methods.response.Transaction;
import org.web3j.protocol.http.HttpService;

import com.ugchain.wallet.server.block.config.BlockConfig;
import com.ugchain.wallet.server.block.dao.BlockDao;
import com.ugchain.wallet.server.block.dao.RedisDao;
import com.ugchain.wallet.server.block.dao.TransactionDao;
import com.ugchain.wallet.server.block.utils.InputUtil;
import com.ugchain.wallet.server.block.vo.BlockRepo;
import com.ugchain.wallet.server.block.vo.Constants;
import com.ugchain.wallet.server.block.vo.TransactionRepo;
import com.ugchain.wallet.server.block.vo.contract.UGToken;


/**
 * @ClassName: BlockServiceImpl
 * @Description: TODO(区块同步数据库服务层接口实现)
 * @author OldDriver
 * @date 2017年6月5日
 * 
 */

@Service
public class BlockServiceImpl implements BlockService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockServiceImpl.class);

    @Autowired
    private BlockConfig appConfig;

    @Autowired
    private TransactionDao transactionDao;

    @Autowired
    private BlockDao blockDao;
    
    @Autowired
    private RedisDao redisDao;
    
    @Autowired
    private InputUtil inputUtil;
    
    private Web3j web3j;

    BigInteger i = BigInteger.ZERO;

    private UGToken ugToken;
    @Override
    public void init() {
	// 初始化web3j对象，创建连接
	web3j = Web3j.build(new HttpService(appConfig.getGethHttpURL()));

	Credentials credentials = null;
	// 加载合约
	ugToken = UGToken.load(appConfig.getUgTokenAddress(), web3j, credentials, GAS_PRICE,
		BigInteger.valueOf(4_700_000));

    }

    public void queryBlock() throws IOException, InterruptedException {
	
	init();

	//查询pending状态的交易
//	SavePendingTransaction savePendingTransaction = new SavePendingTransaction();
//	
//	Thread pThread = new Thread(savePendingTransaction);
//	
//	pThread.start();
	
	// 查询数据库中最新同步的区块
	try {
	    i = new BigInteger(blockDao.maxBlock().getNumber());
//	    i = new BigInteger("158071");
	} catch (Exception e) {
	    i = new BigInteger("3836614");
	    LOGGER.error("query blocktable error");
	}
	while (true) {
	    Request<?, EthBlockNumber> blockNumber = web3j.ethBlockNumber();
	    // 获取当前blockNumber
	    BigInteger number = blockNumber.send().getBlockNumber();
	    
	    if (i.compareTo(number) >= 0) {
		// 当前线程等待2秒
		Thread.sleep(2000);
		continue;
	    }

	    // 根据blockNumber查询block
	    Block block = queryBlock(i);
	    BigInteger bNumber = block.getNumber();
	    // 调用持久层方法保存交易信息
	    String d = "0";
	    if (i.compareTo(BigInteger.ZERO) > 0) {

		// 查询当前区块时间戳
		String timestampRaw = block.getTimestamp().toString();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Long time = Long.valueOf(timestampRaw);
		d = format.format(new Date(time * 1000L));
	    }
	    List<TransactionResult> transactions = block.getTransactions();
	    for (TransactionResult transactionResult : transactions) {
		
		TransactionRepo transactionRepo = new TransactionRepo();
		Transaction transaction = (Transaction) transactionResult.get();
		
		String input = transaction.getInput();
		String to = transaction.getTo();
		int judgeInput = inputUtil.judgeInput(input,to);
		
		if (judgeInput==0) {
		    continue;
		}
		Map<String, Object> resolveInput = null; 
		if (judgeInput!=Constants.coin_type_ETH) {
		    resolveInput = inputUtil.resolveInput(input);
		}	
		if (CollectionUtils.isEmpty(resolveInput)) {
		    transactionRepo.setTo(transaction.getTo());
		    transactionRepo.setCoinType(Constants.coin_type_ETH);
		}else {
		    transactionRepo.setTo((String)resolveInput.get("to"));
		    transactionRepo.setUgtValue((String)resolveInput.get("value"));
		    transactionRepo.setCoinType(Constants.coin_type_UGT);
		}
		transactionRepo.setBlockHash(transaction.getBlockHash());
		
		transactionRepo.setBlockNumber(transaction.getBlockNumber().toString());
		transactionRepo.setFrom(transaction.getFrom());
		transactionRepo.setGas(transaction.getGas().toString());
		transactionRepo.setGasPrice(transaction.getGasPrice().toString());
		transactionRepo.setHash(transaction.getHash());
		transactionRepo.setNonce(transaction.getNonceRaw());
		transactionRepo.setTimeStamp(d);
		transactionRepo.setValue(transaction.getValue().toString());
		try {
		    //未添加事物。。。。。。
		    transactionDao.insert(transactionRepo);
		    redisDao.delete(transaction.getFrom(),transaction.getHash());
		    redisDao.delete(transaction.getTo(),transaction.getHash());
		} catch (Exception e) {
		    LOGGER.error("BlockNumber:" + bNumber + "insert into database false!!!!",e);
		}
	    }
	    LOGGER.info("BlockNumber:" + bNumber + "insert into database");
	    // i自增 1
	    i = i.add(BigInteger.ONE);

	}

    }

    // 查询区块方法,方法中校验是否分叉
    private Block queryBlock(BigInteger i) throws IOException {
	// 读取当前链上blockNumber为i的区块
	Request<?, EthBlock> ethGetBlockByNumber = web3j.ethGetBlockByNumber(DefaultBlockParameter.valueOf(i), true);
	Block block = ethGetBlockByNumber.send().getBlock();

	String parentHash = block.getParentHash();

	// 从数据库中查询数据库中持久化的上一区块hash
	BlockRepo one = blockDao.findOne(i.subtract(BigInteger.ONE).toString());

	if (one == null) {
	    saveBlock(block);
	    return block;
	}
	String hash = one.getHash();

	if (parentHash.equals(hash)) {
	    // 删除区块表中的第一个区块
	    if (i.compareTo(new BigInteger(String.valueOf(100))) > 0) {
		try {
		    i = i.subtract(new BigInteger(String.valueOf(100)));
		    blockDao.remove(i.toString());
		} catch (Exception e) {
		    LOGGER.warn("block table count less than 100");
		}
	    }
	    saveBlock(block);
	    
	    return block;
	} else {
	    i = i.subtract(BigInteger.ONE);
	    // 调用数据库中的方法删除上一区块的交易信息和另一个表中的区块信息
	    // 删除交易信息
	    transactionDao.remove(i.toString());
	    // 删除block表中的分叉区块的数据
	    blockDao.remove(i.toString());
	    
	    return queryBlock(i);
	}
    }

    public void saveBlock(Block block) {
	try {
	    
	BlockRepo blockRepo = new BlockRepo();
	blockRepo.setAuthor(block.getAuthor());
	blockRepo.setDifficulty(block.getDifficulty().toString());
	blockRepo.setExtraData(block.getExtraData());
	blockRepo.setGasLimit(block.getGasLimit().toString());
	blockRepo.setGasUsed(block.getGasUsed().toString());
	blockRepo.setHash(block.getHash());
	blockRepo.setLogsBloom(block.getLogsBloom());
	blockRepo.setMiner(block.getMiner());
	blockRepo.setMixHash(block.getMixHash());
	blockRepo.setNonce(block.getNonceRaw());
	blockRepo.setNumber(block.getNumber().toString());
	blockRepo.setParentHash(block.getParentHash());
	blockRepo.setReceiptsRoot(block.getReceiptsRoot());
	blockRepo.setSealFields(block.getSealFields());
	blockRepo.setSha3Uncles(block.getSha3Uncles());
	blockRepo.setSize(block.getSize().toString());
	blockRepo.setStateRoot(block.getStateRoot());
	blockRepo.setTimestamp(block.getTimestamp().toString());
	blockRepo.setTotalDifficulty(block.getTotalDifficulty().toString());
	blockRepo.setTransactions(block.getTransactions());
	blockRepo.setTransactionsRoot(block.getTransactionsRoot());
	blockRepo.setUncles(block.getUncles());

	blockDao.insert(blockRepo);
	} catch (Exception e) {
	    LOGGER.error("save BLockTable false!!!!");
	    e.printStackTrace();
	}

    }

}
